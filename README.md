# Overview
Mgrep is a Java command-line utility designed to search for large ammounts of literal patterns inside given files. It uses Aho-Corasick algorithm to achieve O(n) time complexity in the length of the input, number of searched patterns is not relevant.


# Usage
`$ mvn package`
`$ java -jar target/mgrep-<version>-standalone.jar -h

