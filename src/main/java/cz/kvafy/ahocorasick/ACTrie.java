package cz.kvafy.ahocorasick;

import cz.kvafy.util.StringSuffixes;

import java.util.*;

/**
 * Aho-Corasick trie for fast multi-pattern search.
 * Created by kvafy on 29.3.14.
 */
public class ACTrie {
    ACTrieNode rootNode;

    public static ACTrie construct(String[] words, boolean caseSensitive) {
        return construct(Arrays.asList(words), caseSensitive);
    }

    public static ACTrie construct(Collection<String> words, boolean caseSensitive) {
        ACTrie acTrie = new ACTrie();

        for(String word : words)
            acTrie.insertWordWithMatchLinksOnly(word, caseSensitive);

        acTrie.createSuffixLinks();

        return acTrie;
    }

    private ACTrie() {
        this.rootNode = new ACTrieNode();
        this.rootNode.value = "";
    }

    private void insertWordWithMatchLinksOnly(String word, boolean caseSensitive) {
        ACTrieNode currentNode = this.rootNode;
        for(int i = 0 ; i < word.length() ; i++) {
            char currentChar = word.charAt(i);
            ACTrieNode nextNode = currentNode.matchFunction.get(currentChar);
            if(nextNode == null) {
                nextNode = new ACTrieNode();
                currentNode.matchFunction.put(currentChar, nextNode);
                if(!caseSensitive)
                    currentNode.matchFunction.put(invertCase(currentChar), nextNode);
                nextNode.parent = currentNode;
                nextNode.value = word.substring(0, i + 1);
            }
            currentNode = nextNode;
        }
        currentNode.isTerminal = true;
    }

    /**
     * For each node N of the trie creates link to other node of the trie such that it represents maximal proper suffix
     * of node N. For root, the value will be <code>null</code>. For other nodes, the value will never be null and
     * at worst case it will point to root if the trie contains no suffix of N other than the empty string.
     */
    private void createSuffixLinks() {
        BFSVisitor suffixLinkCreatingVisitor = new BFSVisitor() {
            @Override
            public void visitNode(ACTrieNode node) {
                node.suffixLink = null;
                // find the largest suffix with defined path in the trie (in worst case it will be root for suffix "")
                for(String suffix : new StringSuffixes(node.value, false)) {
                    if(ACTrie.this.containsPath(suffix)) {
                        node.suffixLink = ACTrie.this.traverse(suffix);
                        break;
                    }
                }
            }
        };
        this.applyBFSVisitor(suffixLinkCreatingVisitor);
    }

    /**
     * Traverses the trie using only match function (forward links).
     * @param input
     *     String to traverse the trie by.
     * @return
     *     The final node after traverse is finished, it the traversing was successful. Otherwise returns
     *     <code>null</code>. Note that for empty string the resulting node is always root.
     */
    private ACTrieNode traverse(String input) {
        ACTrieNode currentNode = this.rootNode;
        for(int i = 0 ; i < input.length() ; i++) {
            currentNode = currentNode.matchFunction.get(input.charAt(i));
            if(currentNode == null)
                return null;
        }
        return currentNode;
    }

    private void applyBFSVisitor(BFSVisitor visitor) {
        LinkedList<ACTrieNode> queue = new LinkedList<ACTrieNode>();
        queue.add(this.rootNode);
        while(!queue.isEmpty()) {
            ACTrieNode node = queue.pollFirst();
            queue.addAll(new HashSet<ACTrieNode>(node.matchFunction.values()));
            visitor.visitNode(node);
        }
    }

    private Character invertCase(char c) {
        if(Character.isUpperCase(c))
            return Character.toLowerCase(c);
        else
            return Character.toUpperCase(c);
    }

    public int getNodeCount() {
        class NodeCountingVisitor extends BFSVisitor {
            int count = 0;
            @Override
            public void visitNode(ACTrieNode node) {
                count++;
            }
        };
        NodeCountingVisitor visitor = new NodeCountingVisitor();
        this.applyBFSVisitor(visitor);
        return visitor.count;
    }

    public boolean containsPath(String word) {
        ACTrieNode finalNode = this.traverse(word);
        return finalNode != null;
    }

    public boolean containsWord(String word) {
        ACTrieNode finalNode = this.traverse(word);
        return finalNode != null && finalNode.isTerminal;
    }

    private static abstract class BFSVisitor {
        public abstract void visitNode(ACTrieNode node);
    }
}


class ACTrieNode {
    boolean isTerminal = false;   // this node corresponds to a keyword in the trie (not an intermediary node)
    String value = null;          // string value of the node (given as concatenated characters from root to this node)
    ACTrieNode parent = null;
    ACTrieNode suffixLink = null; // node that represents suffix with string == this.value and is maximal among all such nodes
    HashMap<Character, ACTrieNode> matchFunction = new HashMap<Character, ACTrieNode>();
}
