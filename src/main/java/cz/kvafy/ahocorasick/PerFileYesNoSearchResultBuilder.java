package cz.kvafy.ahocorasick;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by kvafy on 30.3.14.
 */
public class PerFileYesNoSearchResultBuilder extends PerFileYesNoSearchResult implements SearchResultBuilder {


    public PerFileYesNoSearchResultBuilder(String[] keywords, boolean caseSensitive) {
        this(Arrays.asList(keywords), caseSensitive);
    }

    public PerFileYesNoSearchResultBuilder(Collection<String> keywords, boolean caseSensitive) {
        super(caseSensitive);
        for(String kw : keywords) {
            kw = this.preprocessKeywordByCaseSensitivity(kw);
            this.kwToFileListMap.put(kw, new HashSet<File>());
        }
    }

    @Override
    public void recordOccurrenceOfKeyword(String keyword, File file, String currentLine, int lineNo, int column) {
        keyword = this.preprocessKeywordByCaseSensitivity(keyword);
        if(!this.kwToFileListMap.containsKey(keyword))
            throw new IllegalArgumentException("I wasn't looking for keyword \"" + keyword + "\".");
        else {
            HashSet<File> fileSetForKeyword = this.kwToFileListMap.get(keyword);
            fileSetForKeyword.add(file);
        }
    }

    @Override
    public SearchResult getSearchResult() {
        return (SearchResult)this;
    }
}
