package cz.kvafy.ahocorasick;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * For each keyword keeps track of whether it was encountered at least in one of the input files.
 */
public class OverallYesNoSearchResult extends SearchResult {
    protected Map<String, Boolean> kwToOccurrenceMap;

    OverallYesNoSearchResult(boolean caseSensitive) {
        super(caseSensitive);
        this.kwToOccurrenceMap = new HashMap<String, Boolean>();
    }

    public boolean found(String word) {
        word = this.preprocessKeywordByCaseSensitivity(word);
        if(!this.kwToOccurrenceMap.containsKey(word))
            throw new IllegalArgumentException("I wasn't looking for keyword \"" + word + "\".");
        else
            return kwToOccurrenceMap.get(word);
    }

    @Override
    public void printResultSummary(PrintStream printer) {
        ArrayList<String> sortedKeywords = new ArrayList<String>(this.kwToOccurrenceMap.keySet());
        Collections.sort(sortedKeywords);

        for(String kw : sortedKeywords) {
            String kwFound = this.kwToOccurrenceMap.get(kw) ? "yes" : "no";
            printer.printf("%s: %s\n", kw, kwFound);
        }
    }
}
