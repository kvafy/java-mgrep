package cz.kvafy.ahocorasick;

import java.io.File;
import java.io.PrintStream;
import java.util.*;

/**
 * For each keyword keeps a set of files in which the keyword was found.
 */
public class PerFileYesNoSearchResult extends SearchResult {
    protected HashMap<String, HashSet<File>> kwToFileListMap;

    PerFileYesNoSearchResult(boolean caseSensitive) {
        super(caseSensitive);
        this.kwToFileListMap = new HashMap<String, HashSet<File>>();
    }

    public boolean found(String word, File file) {
        word = this.preprocessKeywordByCaseSensitivity(word);
        if(!this.kwToFileListMap.containsKey(word))
            throw new IllegalArgumentException("I wasn't looking for keyword \"" + word + "\".");
        Set<File> fileSetForWord = this.kwToFileListMap.get(word);
        return fileSetForWord.contains(file);
    }

    @Override
    public void printResultSummary(PrintStream printer) {
        ArrayList<String> sortedKeywords = new ArrayList<String>(this.kwToFileListMap.keySet());
        Collections.sort(sortedKeywords);

        for(String kw : sortedKeywords) {
            ArrayList<File> sortedFiles = new ArrayList<File>(this.kwToFileListMap.get(kw));
            Collections.sort(sortedFiles);
            printer.printf("%s (%d matches)\n", kw, sortedFiles.size());
            for(File file : sortedFiles)
                printer.printf("    %s\n", file.getPath());
        }
    }
}
