package cz.kvafy.ahocorasick;

import java.io.File;
import java.io.PrintStream;

/**
 * Created by kvafy on 9.4.14.
 */
public class GreppingSearchResultBuilder extends GreppingSearchResult implements SearchResultBuilder {
    private PrintStream printer;

    public GreppingSearchResultBuilder(PrintStream printer) {
        this.printer = printer;
    }

    @Override
    public void recordOccurrenceOfKeyword(String keyword, File file, String currentLine, int lineNo, int column) {
        String linePrefix = String.format("%s:%d: ", (file != null ? file.getPath() : "null"), lineNo);
        this.printer.printf("%s%s\n", linePrefix, currentLine);
        String underlineBlankIndentLength = repeatNTimes(" ", linePrefix.length() + column - 1);
        String underlineHighlight = repeatNTimes("=", keyword.length());
        this.printer.printf("%s%s\n", underlineBlankIndentLength, underlineHighlight);
    }

    @Override
    public SearchResult getSearchResult() {
        return SearchResult.EMPTY_SEARCH_RESULT;
    }

    private String repeatNTimes(String base, int repeatCount) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < repeatCount ; i++)
            sb.append(base);
        return sb.toString();
    }
}
