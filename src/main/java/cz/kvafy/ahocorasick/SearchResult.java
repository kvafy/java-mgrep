package cz.kvafy.ahocorasick;

import java.io.PrintStream;

/**
 * Created by kvafy on 29.3.14.
 */
public abstract class SearchResult {
    protected final boolean caseSensitive;

    public SearchResult(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    /**
     * Print the overall search result.
     * This method is intended to have non-empty implementation in search results that
     * accumulate the information and print the result once all input data has been
     * processed.
     * @param printer
     *     To be used for writing the result.
     */
    public abstract void printResultSummary(PrintStream printer);

    /**
     * Convert the keyword according to case-sensitivity of this SearchResult instance.
     * Any subclass should preprocess its keyword argument before actually doing anything with it.
     * @param kw
     *     A keyword.
     * @return
     *     Preprocessed keyword.
     */
    protected String preprocessKeywordByCaseSensitivity(String kw) {
        return this.caseSensitive ? kw : kw.toLowerCase();
    }


    public static final SearchResult EMPTY_SEARCH_RESULT = new SearchResult(true) {
        @Override
        public void printResultSummary(PrintStream printer) {

        }
    };
}