package cz.kvafy.ahocorasick;

import cz.kvafy.util.ContextAwareReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Crawls given AC Trie for given character input stream.
 * Occurrence of each keyword from the AC Trie is recorded via a {@link cz.kvafy.ahocorasick.SearchResultBuilder}.
 */
public class ACTrieCrawler {
    private ACTrie acTrie;
    private ACTrieNode currentNode;
    private SearchResultBuilder resultBuilder;
    private ContextAwareReader inputReader;
    private boolean alreadyCrawledOnce;

    public ACTrieCrawler(ACTrie acTrie, SearchResultBuilder resultBuilder, ContextAwareReader inputReader) {
        this.acTrie = acTrie;
        this.resultBuilder = resultBuilder;
        this.inputReader = inputReader;
        this.alreadyCrawledOnce = false;
    }
    
    public SearchResult crawl() throws IOException {
        if(this.alreadyCrawledOnce)
            throw new IllegalStateException("Cannot crawl more than once.");

        try {
            this.currentNode = this.acTrie.rootNode;

            int nextChar;
            while ((nextChar = this.inputReader.read()) >= 0) {
                Collection<String> matchedKeyWords = this.crawlByCharacter((char) nextChar);
                for (String kw : matchedKeyWords) {
                    this.resultBuilder.recordOccurrenceOfKeyword(
                            kw, this.inputReader.getSrcFile(), this.inputReader.getCurrentLineWithoutNewline(),
                            this.inputReader.getLineNo(), this.inputReader.getColumnNo() - kw.length());
                }
            }
            return this.resultBuilder.getSearchResult();
        }
        finally {
            this.alreadyCrawledOnce = true;
            this.inputReader.close();
        }
    }

    /**
     * Advance by one character and return list of keywords matched after the crawl.
     * @param inputChar Character to advance by.
     * @return Collection of keywords matched after we crawled by the single character. Never null.
     */
    private Collection<String> crawlByCharacter(char inputChar) {
        if(this.currentNode.matchFunction.containsKey(inputChar))
            this.currentNode = this.currentNode.matchFunction.get(inputChar);
        else {
            ACTrieNode suffixNode = this.currentNode.suffixLink;
            while(suffixNode != null) {
                if(suffixNode.matchFunction.containsKey(inputChar)) {
                    this.currentNode = suffixNode.matchFunction.get(inputChar);
                    break;
                }
                else
                    suffixNode = suffixNode.suffixLink;
            }
            if(suffixNode == null) {
                // means we went along the suffix edges all the way up to the root and failed
                // to match the inputChar even there, so let's start over with the matching from root
                this.currentNode = this.acTrie.rootNode;
            }
        }

        return this.matchedWordsForNode(this.currentNode);
    }

    private ArrayList<String> tmpMatchedWordsList = new ArrayList<String>(); // to avoid frequent garbage creation
    private ArrayList<String> matchedWordsForNode(ACTrieNode node) {
        tmpMatchedWordsList.clear();
        while(node != null) {
            if(node.isTerminal)
                tmpMatchedWordsList.add(node.value);
            node = node.suffixLink;
        }
        return tmpMatchedWordsList;
    }
}
