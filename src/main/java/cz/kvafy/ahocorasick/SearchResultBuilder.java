package cz.kvafy.ahocorasick;

import java.io.File;

/**
 * Created by kvafy on 29.3.14.
 */
public interface SearchResultBuilder {
    public abstract void recordOccurrenceOfKeyword(String keyword, File file, String currentLine, int lineNo, int column);
    public abstract SearchResult getSearchResult();
}
