package cz.kvafy.ahocorasick;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;

/**
 * After the search is complete, provides information whether given keyword was contained in any one of the input files.
 * Created by kvafy on 29.3.14.
 */
public class OverallYesNoSearchResultBuilder extends OverallYesNoSearchResult implements SearchResultBuilder {

    public OverallYesNoSearchResultBuilder(String[] keywords, boolean caseSensitive) {
        this(Arrays.asList(keywords), caseSensitive);
    }

    public OverallYesNoSearchResultBuilder(Collection<String> keywords, boolean caseSensitive) {
        super(caseSensitive);

        for(String keyword : keywords) {
            keyword = this.preprocessKeywordByCaseSensitivity(keyword);
            this.kwToOccurrenceMap.put(keyword, false);
        }
    }

    @Override
    public void recordOccurrenceOfKeyword(String keyword, File file, String line, int lineNo, int column) {
        keyword = this.preprocessKeywordByCaseSensitivity(keyword);
        if(!this.kwToOccurrenceMap.containsKey(keyword))
            throw new IllegalArgumentException("I wasn't looking for keyword \"" + keyword + "\".");
        else
            this.kwToOccurrenceMap.put(keyword, true);
    }

    @Override
    public SearchResult getSearchResult() {
        return (OverallYesNoSearchResult)this;
    }
}
