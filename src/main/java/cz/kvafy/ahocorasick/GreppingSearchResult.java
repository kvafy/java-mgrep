package cz.kvafy.ahocorasick;

import java.io.PrintStream;

/**
 * Created by kvafy on 9.4.14.
 */
public class GreppingSearchResult extends SearchResult {
    GreppingSearchResult() {
        super(true); // just to pass the parent something for case-sensitiveness
    }

    @Override
    public void printResultSummary(PrintStream printer) {
    }
}
