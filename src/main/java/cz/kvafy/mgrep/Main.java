package cz.kvafy.mgrep;

import cz.kvafy.ahocorasick.*;
import cz.kvafy.util.ContextAwareReader;
import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

// TODO list
//   [x] case-insensitive support
//   [x] tests
//   [x] cmdline args parsing
//   [x] directory structure crawling with ext white list
//   [ ] other SearchResultBuilders (two more)
//   [x] ContextAwareReader


public class Main {
    private static final int EXIT_OK = 0;
    private static final int EXIT_INCORRECT_ARGUMENTS = 1;
    private static final int EXIT_IO_ERROR = 2;

    public static void main(String[] argv) {
        CommandLine commandLine = processAndValidateCommandLine(argv);
;
        if(commandLine.hasOption("help")) {
            printHelp();
            System.exit(EXIT_OK);
        }

        try {
            // load command line options
            boolean caseSensitive = !commandLine.hasOption("case-insensitive");

            List<File> files = null;
            if(commandLine.hasOption("input-files-file"))
                files = loadFileListFromFile(new File(commandLine.getOptionValue("input-files-file")));
            else { // --recurse option
                Set<String> extWhiteList = null;
                if(commandLine.hasOption("accepted-extensions")) {
                    String[] extWhiteListArray = commandLine.getOptionValue("accepted-extensions").split(",");
                    extWhiteList = new HashSet<String>(Arrays.asList(extWhiteListArray));
                }
                files = loadFileListByRecursiveCrawl(new File(commandLine.getOptionValue("recurse")), extWhiteList);
            }

            List<String> keywords = null;
            if(commandLine.hasOption("keywords-file"))
                keywords = loadNonblankLinesFromTextFile(new File(commandLine.getOptionValue("keywords-file")));
            else
                keywords = commandLine.getArgList();

            SearchResultBuilder searchResultBuilder;
            if(commandLine.hasOption("total"))
                searchResultBuilder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);
            else if(commandLine.hasOption("per-file"))
                searchResultBuilder = new PerFileYesNoSearchResultBuilder(keywords, caseSensitive);
            else
                searchResultBuilder = new GreppingSearchResultBuilder(System.out);


            // scan the files for keywords
            ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);
            for(File file : files) {
                try {
                    ContextAwareReader reader = new ContextAwareReader(file);
                    ACTrieCrawler trieCrawler = new ACTrieCrawler(acTrie, searchResultBuilder, reader);
                    trieCrawler.crawl();
                } catch (IOException e) {
                    System.err.println("IO error reading an input file: " + e);
                }
            }

            // present the search results
            SearchResult searchResult = searchResultBuilder.getSearchResult();
            searchResult.printResultSummary(System.out);
            System.exit(EXIT_OK);
        }
        catch(IOException ioex) {
            System.err.println("Encountered an IO error while loading the input files: " + ioex);
            System.exit(EXIT_IO_ERROR);
        }
    }

    private static CommandLine processAndValidateCommandLine(String[] argv) {
        CommandLine commandLine = processCommandLine(argv);
        validateCommandLine(commandLine);
        return commandLine;
    }

    private static CommandLine processCommandLine(String[] argv) {
        Options options = createCommandLineOptions();
        CommandLineParser parser = new BasicParser();
        try {
            return parser.parse(options, argv);
        } catch (ParseException e) {
            System.err.println("Failed to parse command line arguments: " + e.getMessage());
            System.exit(EXIT_INCORRECT_ARGUMENTS);
            return null; // dummy for compiler
        }
    }

    private static void validateCommandLine(CommandLine commandLine) {
        if(commandLine.hasOption("help"))
            return;

        String errorMsg = null;
        if(!(commandLine.hasOption("recurse") ^ commandLine.hasOption("input-files-file")))
            errorMsg = "Exactly one of the options -r or -f needs to be specified.";
        else if(!(commandLine.hasOption("keywords-file") ^ commandLine.getArgList().size() > 0))
            errorMsg = "Exactly of the options -k <file> or keyword list.";
        else if(commandLine.hasOption("total") && commandLine.hasOption("per-file"))
            errorMsg = "At most one of the options -t or -p may be used.";

        if(errorMsg != null) {
            System.err.println("Error processing arguments: " + errorMsg);
            System.exit(EXIT_INCORRECT_ARGUMENTS);
        }
    }

    private static void printHelp() {
        String usage = "\n"
                     + "mgrep [-itp] -f <input_files_file> -k <keywords_file>\n"
                     + "mgrep [-itp] -f <input_files_file> KEYWORD...\n"
                     + "mgrep [-itp] -r <base_dir> [-e <file_extensions>] -k <keywords_file>\n"
                     + "mgrep [-itp] -r <base_dir> [-e <file_extensions>] KEYWORD...\n"
                     + "mgrep -h";
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(80, usage, null, createCommandLineOptions(), null);
    }

    private static Options createCommandLineOptions() {
        /*
         *   prog -h [anything else]
         *   prog [-itp] -f <input_files_file> -k <keyword_file>
         *   prog [-itp] -f <input_files_file> keywords...
         *   prog [-itp] -r <directory> [-e <accepted_file_extensions>] -k <keyword_file>
         *   prog [-itp] -r <directory> [-e <accepted_file_extensions>] keywords...
         */

        Option help = new Option(
                "h", "help", false, "Print this help.");
        Option caseInsensitiveOption = new Option(
                "i", "case-insensitive", false, "Use case-insensitive pattern matching. False by default.");
        Option resultTotal = new Option(
                "t", "total", false, "Suppress standard output format. Instead, for each keyword print only whether"
                + " it was contained in at least one of the input files.");
        Option resultPerFile = new Option(
                "p", "per-file", false, "Suppress standard output format. Instead, for each keyword print the list of"
                + " files it was encountered in.");
        Option inputFilesFile = new Option(
                "f", "input-files-file", true, "Read paths to input files from this file, one file per line.");
        Option recurseDirectory = new Option(
                "r", "recurse", true, "Recursively perform the multi-pattern search in all files in the given"
                + " directory. List of accepted file extensions can be supplied via -e option.");
        Option acceptedExtensions = new Option(
                "e", "accepted-extensions", true, "Comma-separated list of accepted file extensions."
                + " This option can be used only with -r.");
        Option keywordsFile = new Option(
                "k", "keywords-file", true, "File containing searched keywords, one keyword per line.");

        Options options = new Options();
        options.addOption(help);
        options.addOption(caseInsensitiveOption);
        options.addOption(resultTotal);
        options.addOption(resultPerFile);
        options.addOption(inputFilesFile);
        options.addOption(recurseDirectory);
        options.addOption(acceptedExtensions);
        options.addOption(keywordsFile);
        return options;
    }

    private static List<File> loadFileListFromFile(File srcFile) throws IOException {
        ArrayList<File> fileList = new ArrayList<File>();
        for(String fileName : loadNonblankLinesFromTextFile(srcFile)) {
            fileList.add(new File(fileName));
        }
        return fileList;
    }

    private static List<File> loadFileListByRecursiveCrawl(File baseDir, Set<String> extensionWhiteList) throws IOException {
        ArrayList<File> fileList = new ArrayList<File>();
        LinkedList<File> dfsQue = new LinkedList<File>();
        dfsQue.add(baseDir);
        while(!dfsQue.isEmpty()) {
            File fileSystemEntry = dfsQue.pollFirst();
            if(fileSystemEntry.isDirectory()) {
                File[] directoryContent = fileSystemEntry.listFiles();
                if(directoryContent == null)
                    System.err.println("Could not list directory \"" + fileSystemEntry + "\"");
                else
                    dfsQue.addAll(0, Arrays.asList(fileSystemEntry.listFiles()));
            }
            else if(extensionWhiteList == null || extensionWhiteList.contains(fileExtension(fileSystemEntry)))
                fileList.add(fileSystemEntry);
        }
        return fileList;
    }

    private static String fileExtension(File file) {
        String name = file.getName();
        if(name.contains("."))
            return name.substring(name.lastIndexOf('.') + 1);
        else return null;
    }

    private static List<String> loadNonblankLinesFromTextFile(File file) throws IOException {
        BufferedReader reader = null;
        try {
            LinkedList<String> nonBlankLines = new LinkedList<String>();
            reader = new BufferedReader(new FileReader(file));
            String line;
            while((line = reader.readLine()) != null)
                if(!line.isEmpty())
                    nonBlankLines.add(line);
            return nonBlankLines;
        }
        finally {
            if(reader != null)
                reader.close();
        }
    }
}
