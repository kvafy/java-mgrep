package cz.kvafy.util;

import java.io.*;

/**
 * Accepts a file and reads it as character input.
 * At any time the reader knows line number and column number it is currently reading from as well as the whole current
 * line.
 */
public class ContextAwareReader {
    private File srcFile;
    private BufferedReader reader;
    private int columnNo;
    private int lineNo;
    private String currentLine;

    public ContextAwareReader(File file) throws IOException {
        this.srcFile = file;
        this.reader = new BufferedReader(new FileReader(file));
        this.readNextLine();
        this.lineNo = 1;
    }

    /** Used for testing only (doesn't provide <code>srcFile</code>). */
    public ContextAwareReader(String str) throws IOException {
        this.srcFile = null;
        this.reader = new BufferedReader(new StringReader(str));
        this.readNextLine();
        this.lineNo = 1;
    }

    private void readNextLine() throws IOException {
        String lineRead = this.reader.readLine();
        this.currentLine = (lineRead != null) ? (lineRead + "\n") : null;
        this.columnNo = 1;
        this.lineNo++;
    }

    public int read() throws IOException {
        if(this.currentLine != null && (columnNo - 1) >= this.currentLine.length())
            this.readNextLine();
        if(this.currentLine == null)
            return -1;
        else
            return this.currentLine.charAt(columnNo++ - 1);
    }

    public void close() throws IOException {
        this.reader.close();
    }

    public File getSrcFile() {
        return this.srcFile;
    }

    public String getCurrentLine() {
        return this.currentLine;
    }

    public String getCurrentLineWithoutNewline() {
        String result = getCurrentLine();
        if(result != null)
            result = result.substring(0, result.length() - 1);
        return result;
    }

    public int getLineNo() {
        return this.lineNo;
    }

    public int getColumnNo() {
        return this.columnNo;
    }
}
