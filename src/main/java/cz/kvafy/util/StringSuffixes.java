package cz.kvafy.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator that offers all suffixes of a given string, starting from the longest.
 * Created by kvafy on 29.3.14.
 */
public class StringSuffixes implements Iterable<String> {
    private String str;
    private boolean includeFullSuffix;

    public StringSuffixes(String str) {
        this(str, true);
    }

    public StringSuffixes(String str, boolean includeFullSuffix) {
        this.str = str;
        this.includeFullSuffix = includeFullSuffix;
    }

    @Override
    public Iterator<String> iterator() {
        return new StringSuffixIterator(this.str, this.includeFullSuffix);
    }
}

class StringSuffixIterator implements Iterator<String> {
    private String iteratedString;
    private int index;

    public StringSuffixIterator(String str, boolean includeFullSuffix) {
        this.iteratedString = str;
        this.index = includeFullSuffix ? 0 : 1;
    }

    @Override
    public boolean hasNext() {
        return this.index <= this.iteratedString.length();
    }

    @Override
    public String next() {
        if(!this.hasNext())
            throw new NoSuchElementException("All string prefixes have been iterated over.");
        return this.iteratedString.substring(this.index++);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
