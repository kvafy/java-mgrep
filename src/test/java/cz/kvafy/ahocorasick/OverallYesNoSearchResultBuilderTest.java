package cz.kvafy.ahocorasick;

import junit.framework.TestCase;

/**
 * Created by kvafy on 12.4.14.
 */
public class OverallYesNoSearchResultBuilderTest extends TestCase {
    public void testRecordOccurrenceOfSingleKeyword() {
        String[] keywords = {"foo", "bar"};
        boolean caseSensitive = true;

        OverallYesNoSearchResultBuilder builder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);
        builder.recordOccurrenceOfKeyword("foo", null, null, 0, 0);
        OverallYesNoSearchResult result = (OverallYesNoSearchResult)builder.getSearchResult();

        assertTrue(result.found("foo"));
        assertFalse(result.found("bar"));
    }

    public void testRecordOccurrenceOfSingleKeywordTwice() {
        String[] keywords = {"foo", "bar"};
        boolean caseSensitive = true;

        OverallYesNoSearchResultBuilder builder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);
        builder.recordOccurrenceOfKeyword("foo", null, null, 0, 0);
        builder.recordOccurrenceOfKeyword("foo", null, null, 0, 0);
        OverallYesNoSearchResult result = (OverallYesNoSearchResult)builder.getSearchResult();

        assertTrue(result.found("foo"));
        assertFalse(result.found("bar"));
    }

    public void testRecordOccurrenceOfSingleKeywordCaseInsensitive() {
        String[] keywords = {"foo", "bar"};
        boolean caseSensitive = false;

        OverallYesNoSearchResultBuilder builder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);
        builder.recordOccurrenceOfKeyword("FOO", null, null, 0, 0); // counts as "foo"
        OverallYesNoSearchResult result = (OverallYesNoSearchResult)builder.getSearchResult();

        assertTrue(result.found("foo"));
        assertTrue(result.found("FOO"));
        assertTrue(result.found("fOO"));
        assertFalse(result.found("bar"));
        assertFalse(result.found("BAR"));
    }

    public void testUnknownKeywordThrowsException() {
        String[] keywords = {"foo"};
        boolean caseSensitive = true;

        OverallYesNoSearchResultBuilder builder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);
        try {
            builder.recordOccurrenceOfKeyword("bar", null, null, 0, 0);
            fail("IllegalArgumentException should have been thrown.");
        }
        catch(IllegalArgumentException expected) {

        }
    }

    public void testKeywordWithCaseMissmatchThrowsException() {
        String[] keywords = {"foo"};
        boolean caseSensitive = true;

        OverallYesNoSearchResultBuilder builder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);
        try {
            builder.recordOccurrenceOfKeyword("FOO", null, null, 0, 0);
            fail("IllegalArgumentException should have been thrown.");
        }
        catch(IllegalArgumentException expected) {

        }
    }
}
