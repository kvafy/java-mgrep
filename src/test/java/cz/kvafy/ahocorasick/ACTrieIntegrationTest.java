package cz.kvafy.ahocorasick;

import cz.kvafy.util.ContextAwareReader;
import junit.framework.TestCase;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by kvafy on 30.3.14.
 */
public class ACTrieIntegrationTest extends TestCase {
    private static final String LINE1 = "In addition to being the most intelligent Java IDE, IntelliJ IDEA does provide";
    private static final String LINE2 = " out-of-the-box support for web, enterprise and mobile frameworks, which all together bring you an";
    private static final String LINE3 = " unparalleled user experience.";
    private static final String LINE4 = "";
    private static final String LINE5 = "In the mathematical theory of directed graphs, a graph is said to be strongly";
    private static final String LINE6 = " connected if every vertex is reachable from every other vertex. The strongly";
    private static final String LINE7 = " connected components of an arbitrary directed graph form a partition into";
    private static final String LINE8 = " subgraphs that are themselves strongly connected. It is possible to test the";
    private static final String LINE9 = " strong connectivity of a graph, or to find its strongly connected components,";
    private static final String LINE10 = " in linear time.";
    private static final String TEXT = LINE1 + "\n"
                                     + LINE2 + "\n"
                                     + LINE3 + "\n"
                                     + LINE4 + "\n"
                                     + LINE5 + "\n"
                                     + LINE6 + "\n"
                                     + LINE7 + "\n"
                                     + LINE8 + "\n"
                                     + LINE9 + "\n"
                                     + LINE10 + "\n";

    public void testYesNoFindingCaseSensitive() throws Exception {
        String[] keywords = new String[] {"IDE", "IDEA", "enterprise", "duck", "non-sense"};
        boolean caseSensitive = true;
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);

        ContextAwareReader stringReader = new ContextAwareReader(TEXT);
        SearchResultBuilder yesNoSearchResultBuilder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, yesNoSearchResultBuilder, stringReader);
        OverallYesNoSearchResult overallYesNoSearchResult = (OverallYesNoSearchResult)crawler.crawl();

        assertTrue(overallYesNoSearchResult.found("IDE"));
        assertTrue(overallYesNoSearchResult.found("IDEA"));
        assertTrue(overallYesNoSearchResult.found("enterprise"));
        assertFalse(overallYesNoSearchResult.found("duck"));
        assertFalse(overallYesNoSearchResult.found("non-sense"));
    }

    public void testYesNoFindingCaseInsensitive() throws Exception {
        String[] keywords = new String[] {"ide", "idea", "enterprise", "duck", "non-sense"};
        boolean caseSensitive = false;
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);

        ContextAwareReader stringReader = new ContextAwareReader(TEXT);
        SearchResultBuilder yesNoSearchResultBuilder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, yesNoSearchResultBuilder, stringReader);
        OverallYesNoSearchResult overallYesNoSearchResult = (OverallYesNoSearchResult)crawler.crawl();

        assertTrue(overallYesNoSearchResult.found("ide"));
        assertTrue(overallYesNoSearchResult.found("idea"));
        assertTrue(overallYesNoSearchResult.found("enterprise"));
        assertFalse(overallYesNoSearchResult.found("duck"));
        assertFalse(overallYesNoSearchResult.found("non-sense"));
    }

    public void testYesNoFindingMultipleSuffixes() throws Exception {
        String[] keywords = new String[] {
                "unparalleled user experience", "user experience", "experience", "ience", "ence", "nce", "ce", "e"
        };
        boolean caseSensitive = true;
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);

        ContextAwareReader stringReader = new ContextAwareReader(TEXT);
        SearchResultBuilder yesNoSearchResultBuilder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, yesNoSearchResultBuilder, stringReader);
        OverallYesNoSearchResult overallYesNoSearchResult = (OverallYesNoSearchResult)crawler.crawl();

        for(String kw : keywords)
            assertTrue(overallYesNoSearchResult.found(kw));
    }

    public void testYesNoFindingMultiplePrefixes() throws Exception {
        String[] keywords = new String[] {
                "unparalleled user experience", "unparalleled user", "unparalleled", "unparallel", "unparall", "unpara",
                "unpar", "unpa", "unp", "un", "u"
        };
        boolean caseSensitive = true;
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);

        ContextAwareReader stringReader = new ContextAwareReader(TEXT);
        SearchResultBuilder yesNoSearchResultBuilder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, yesNoSearchResultBuilder, stringReader);
        OverallYesNoSearchResult overallYesNoSearchResult = (OverallYesNoSearchResult)crawler.crawl();

        for(String kw : keywords)
            assertTrue(overallYesNoSearchResult.found(kw));
    }

    public void testYesNoFindingMultipleSubstrings() throws Exception {
        String[] keywords = new String[] {
                "unparalleled user experience", "user", "se"
        };
        boolean caseSensitive = true;
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);

        ContextAwareReader stringReader = new ContextAwareReader(TEXT);
        SearchResultBuilder yesNoSearchResultBuilder = new OverallYesNoSearchResultBuilder(keywords, caseSensitive);

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, yesNoSearchResultBuilder, stringReader);
        OverallYesNoSearchResult overallYesNoSearchResult = (OverallYesNoSearchResult)crawler.crawl();

        for(String kw : keywords)
            assertTrue(overallYesNoSearchResult.found(kw));
    }

    public void testStandardGreppingNoMatchCaseSensitive() throws Exception {
        final String SEARCHED_KW = "mAthEmAtIcAl";
        boolean caseSensitive = true;

        String[] keywords = new String[] {SEARCHED_KW};
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);
        ContextAwareReader stringReader = new ContextAwareReader(TEXT);

        SearchResultBuilder mockSearchResultBuilder = new SearchResultBuilder() {
            @Override
            public void recordOccurrenceOfKeyword(String keyword, File file, String currentLine, int lineNo, int column) {
                fail("The keyword shouldn't be found because of case-sensitivity.");
            }

            @Override
            public SearchResult getSearchResult() {
                return null;
            }
        };

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, mockSearchResultBuilder, stringReader);
        crawler.crawl();
    }

    public void testStandardGreppingNoMatchCaseInsensitive() throws Exception {
        final String SEARCHED_KW = "mathematical_suffix";
        boolean caseSensitive = false;

        String[] keywords = new String[] {SEARCHED_KW};
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);
        ContextAwareReader stringReader = new ContextAwareReader(TEXT);

        SearchResultBuilder mockSearchResultBuilder = new SearchResultBuilder() {
            @Override
            public void recordOccurrenceOfKeyword(String keyword, File file, String currentLine, int lineNo, int column) {
                fail("There should be no match because of the case-sensitivity.");
            }

            @Override
            public SearchResult getSearchResult() {
                return null;
            }
        };

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, mockSearchResultBuilder, stringReader);
        crawler.crawl();
    }


    public void testStandardGreppingSingleLine() throws Exception {
        final String SEARCHED_KW = "mathematical";
        final String EXPECTED_LINE = LINE5;
        final int EXPECTED_LINE_NO = 5;
        final int EXPECTED_COLUMN_NO = EXPECTED_LINE.indexOf(SEARCHED_KW) + 1;
        final int EXPECTED_NUMBER_OF_MATCHES = 1;
        final AtomicInteger numberOfMatches = new AtomicInteger(0); // used this type just because it is mutable

        String[] keywords = new String[] {SEARCHED_KW};
        boolean caseSensitive = true;
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);

        ContextAwareReader stringReader = new ContextAwareReader(TEXT);

        SearchResultBuilder mockSearchResultBuilder = new SearchResultBuilder() {
            @Override
            public void recordOccurrenceOfKeyword(String keyword, File file, String currentLine, int lineNo, int column) {
                assertEquals(SEARCHED_KW, keyword);
                assertEquals(EXPECTED_LINE, currentLine);
                assertEquals(EXPECTED_LINE_NO, lineNo);
                assertEquals(EXPECTED_COLUMN_NO, column);
                numberOfMatches.incrementAndGet();
            }

            @Override
            public SearchResult getSearchResult() {
                return null;
            }
        };

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, mockSearchResultBuilder, stringReader);
        crawler.crawl();

        assertEquals(EXPECTED_NUMBER_OF_MATCHES, numberOfMatches.intValue());
    }

    public void testStandardGreppingMultipleLines() throws Exception {
        boolean caseSensitive = true;
        final String SEARCHED_KW = "connected";
        final String[] EXPECTED_LINES = {LINE6, LINE7, LINE8, LINE9};
        final int[] EXPECTED_LINE_NOS = {6, 7, 8, 9};
        final int[] EXPECTED_COLUMN_NOS = {LINE6.indexOf(SEARCHED_KW) + 1, LINE7.indexOf(SEARCHED_KW) + 1,
                                           LINE8.indexOf(SEARCHED_KW) + 1, LINE9.indexOf(SEARCHED_KW) + 1};
        final int EXPECTED_NUMBER_OF_MATCHES = EXPECTED_LINES.length;
        final AtomicInteger numberOfMatches = new AtomicInteger(0); // used this type just because it is mutable

        String[] keywords = new String[] {SEARCHED_KW};
        ACTrie acTrie = ACTrie.construct(keywords, caseSensitive);
        ContextAwareReader stringReader = new ContextAwareReader(TEXT);

        SearchResultBuilder mockSearchResultBuilder = new SearchResultBuilder() {
            @Override
            public void recordOccurrenceOfKeyword(String keyword, File file, String currentLine, int lineNo, int column) {
                int matchNo = numberOfMatches.intValue();
                assertEquals(SEARCHED_KW, keyword);
                assertEquals(EXPECTED_LINES[matchNo], currentLine);
                assertEquals(EXPECTED_LINE_NOS[matchNo], lineNo);
                assertEquals(EXPECTED_COLUMN_NOS[matchNo], column);
                numberOfMatches.incrementAndGet();
            }

            @Override
            public SearchResult getSearchResult() {
                return null;
            }
        };

        ACTrieCrawler crawler = new ACTrieCrawler(acTrie, mockSearchResultBuilder, stringReader);
        crawler.crawl();

        assertEquals(EXPECTED_NUMBER_OF_MATCHES, numberOfMatches.intValue());
    }
}
