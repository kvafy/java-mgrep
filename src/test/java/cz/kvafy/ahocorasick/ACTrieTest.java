package cz.kvafy.ahocorasick;

import junit.framework.TestCase;

/**
 * Created by kvafy on 30.3.14.
 */
public class ACTrieTest extends TestCase {
    public void testConstructEmptyTrie() throws Exception {
        String[] noKW = new String[0];
        ACTrie acTrieNoKW = ACTrie.construct(noKW, true);
        assertEquals(1 + 0, acTrieNoKW.getNodeCount());
    }

    public void testConstructForSingleKW() throws Exception {
        String[] singleKW = new String[] {"abc"};
        ACTrie acTrieSingleKW = ACTrie.construct(singleKW, true);
        assertEquals(1 + 3, acTrieSingleKW.getNodeCount());
    }

    public void testConstructForRepetitiveKW() throws Exception {
        String[] singleKWTwice = new String[] {"ab", "ab"};
        ACTrie acTrieSingleKWTwice = ACTrie.construct(singleKWTwice, true);
        assertEquals(1 + 2, acTrieSingleKWTwice.getNodeCount());
    }

    public void testConstructCaseSensitive() throws Exception {
        String[] keywords = new String[] {"abc", "ABC"};
        ACTrie acTrie = ACTrie.construct(keywords, true);
        assertEquals(1 + 3 + 3, acTrie.getNodeCount());
    }

    public void testConstructCaseInsensitive() throws Exception {
        String[] keywords = new String[] {"abc", "ABC", "AbC"};
        ACTrie acTrie = ACTrie.construct(keywords, false);
        assertEquals(1 + 3, acTrie.getNodeCount());
    }

    public void testContainsEmptyPrefix() throws Exception {
        String[] noKW = new String[0];
        ACTrie acTrieNoKW = ACTrie.construct(noKW, true);
        assertTrue(acTrieNoKW.containsPath(""));
        assertFalse(acTrieNoKW.containsWord(""));

        String[] emptyStringKW = new String[] {""};
        ACTrie acTrieEmptyString = ACTrie.construct(emptyStringKW, true);
        assertTrue(acTrieEmptyString.containsPath(""));
        assertTrue(acTrieEmptyString.containsWord(""));
    }

    public void testContainsAllPrefixes() throws Exception {
        String[] keywords = new String[] {"hello world", "big bang"};
        ACTrie acTrie = ACTrie.construct(keywords, true);
        for(String kw : keywords) {
            for(int i = 0 ; i <= kw.length() ; i++) {
                String prefix = kw.substring(0, i);
                assertTrue(acTrie.containsPath(prefix));
            }
        }
    }

    public void testContainsOnlyKeywords() throws Exception {
        String[] keywords = new String[] {"hello world", "big bang"};
        ACTrie acTrie = ACTrie.construct(keywords, true);
        for(String kw : keywords)
            assertTrue(acTrie.containsWord(kw));
        for(String kw : keywords) {
            for(int i = 0 ; i < kw.length() ; i++) {
                String prefix = kw.substring(0, i);
                assertFalse(acTrie.containsWord(prefix));
            }
        }
    }

    public void testContainsPathCaseSensitive() throws Exception {
        String[] keywords = new String[] {"hello world"};
        ACTrie acTrie = ACTrie.construct(keywords, true);
        assertTrue(acTrie.containsPath("hello world"));
        assertFalse(acTrie.containsPath("Hello world"));
    }

    public void testContainsPathCaseInsensitive() throws Exception {
        String[] keywords = new String[] {"hello world"};
        ACTrie acTrie = ACTrie.construct(keywords, false);
        assertTrue(acTrie.containsPath("hello world"));
        assertTrue(acTrie.containsPath("Hello world"));
        assertTrue(acTrie.containsPath("Hello World"));
        assertTrue(acTrie.containsPath("hello world".toUpperCase()));
    }
}
